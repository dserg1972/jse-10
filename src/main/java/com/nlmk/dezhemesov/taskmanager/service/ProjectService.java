package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.repository.ProjectRepository;

import java.util.List;

/**
 * Служба проектов
 */
public class ProjectService {

    /**
     * Репозиторий проектов
     */
    private final ProjectRepository projectRepository;

    /**
     * Конструктор
     *
     * @param projectRepository репозиторий проектов
     */
    public ProjectService(final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создание проекта
     *
     * @param name имя проекта
     * @return созданный проект
     */
    public Project create(final String name) {
        return projectRepository.create(name);
    }

    /**
     * Очистка репозитория
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Получение списка проектов
     *
     * @return список проектов
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Получение проекта по индексу в репозитории
     *
     * @param index индекс проекта в репозитории
     * @return найденный проект либо null, если проет не найден
     */
    public Project findByIndex(final int index) {
        if (index < 0 || index > projectRepository.size())
            return null;
        return projectRepository.findByIndex(index);
    }

    /**
     * Поиск проекта по имени
     *
     * @param name имя проекта
     * @return найденный проект либо null, если проет не найден
     */
    public Project findByName(final String name) {
        if (name == null || name.equals(""))
            return null;
        return projectRepository.findByName(name);
    }

    /**
     * Поиск проекта по идентификатору
     *
     * @param id идентификатор
     * @return найденный проект либо null, если проет не найден
     */
    public Project findById(final Long id) {
        if (id == null)
            return null;
        return projectRepository.findById(id);
    }

}
