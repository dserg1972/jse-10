package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.repository.ProjectRepository;
import com.nlmk.dezhemesov.taskmanager.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

/**
 * Сервис с общей бизнес-логикой для проектов и задач
 */
public class ProjectTaskService {

    /**
     * Репозиторий проектов
     */
    private final ProjectRepository projectRepository;

    /**
     * Репозиторий задач
     */
    private final TaskRepository taskRepository;

    /**
     * Конструктор
     *
     * @param projectRepository репозиториий проектов
     * @param taskRepository    репозиторий задач
     */
    public ProjectTaskService(final ProjectRepository projectRepository, final TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Поиск задач по идентификатору проекта
     *
     * @param projectId идентификатор проекта
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }

    /**
     * Присоединение задачу к проекту
     *
     * @param projectId идентификатор проекта, в который необходимо добавить задачу
     * @param taskId    идентификатор добавляемой задачи
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(project.getId(), project.getName());
        return task;
    }

    /**
     * Отстоединение задачи от проекта
     *
     * @param projectId идентификатор проекта
     * @param taskId    идентификатор задачи
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        Task task = taskRepository.findByProjectIdAndTaskId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null, null);
        return task;
    }

}
