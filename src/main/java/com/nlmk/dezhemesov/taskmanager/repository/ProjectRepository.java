package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Хранилище проектов
 */
public class ProjectRepository {

    /**
     * Список проектов
     */
    private final List<Project> projects = new ArrayList<>();

    {
        create("PROJECT1", "Описание проекта 1");
        create("PROJECT2");
    }

    /**
     * Создание проекта с именем и добавление в хранилище
     *
     * @param name имя проекта
     * @return созданный проект
     */
    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    /**
     * Создание проекта с именем и описанием и добавление в хранилище
     *
     * @param name        имя проекта
     * @param description описание проекта
     * @return созданный проект
     */
    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Возврат ссылки на хранилище
     *
     * @return ссылка на хранилище
     */
    public List<Project> findAll() {
        return projects;
    }

    /**
     * Поиск проекта в хранилище по индексу
     *
     * @param index индекс проекта в хранилище
     * @return найденный проект либо null, если проект не найден либо неправильный индекс
     */
    public Project findByIndex(final int index) {
        if (index < 0 || index > projects.size())
            return null;
        return projects.get(index);
    }

    /**
     * Поиск проекта в хранилище по имени
     *
     * @param name имя проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findByName(final String name) {
        Optional<Project> optionalProject = projects.stream().filter(p -> name.equals(p.getName())).findFirst();
        return optionalProject.orElse(null);
    }

    /**
     * Поиск проекта в хранилище по идентификатору
     *
     * @param id идентификатор проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findById(final Long id) {
        Optional<Project> optionalProject = projects.stream().filter(p -> id.equals(p.getId())).findFirst();
        return optionalProject.orElse(null);
    }

    /**
     * Получение размера хранилища
     *
     * @return Размер хранилища
     */
    public int size() {
        return projects.size();
    }

    public static void main(final String[] args) {
        ProjectRepository repository = new ProjectRepository();
        Project project = repository.findByName("PROJECT2");
        System.out.println(project);
    }

}
