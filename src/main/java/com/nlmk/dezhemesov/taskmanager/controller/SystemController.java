package com.nlmk.dezhemesov.taskmanager.controller;

/**
 * Контроллер системных команд
 */
public class SystemController extends AbstractController {

    /**
     * Вывод строки-приветствия
     */
    public static void displayWelcome() {
        System.out.println("*** WELCOME TO THE TASK MANAGER ***");
    }

    /**
     * Сведения о программе
     */
    public int displayAbout() {
        System.out.println("Автор: Сергей Дежемесов");
        System.out.println("       dserg1972@gmail.com");
        return 0;
    }

    /**
     * Сведенеия о версии
     */
    public int displayVersion() {
        System.out.println("Версия: 1.0.0");
        return 0;
    }

    /**
     * Перечень команд
     */
    public int displayHelp() {
        System.out.println("Запуск: java -jar taskmanager.jar [команда]");
        System.out.println("Команды:");
        System.out.println("  about   - вывод информации о проекте");
        System.out.println("  help    - вывод помощи по ключам запуска и командам");
        System.out.println("  version - вывод информации о версии");
        System.out.println("  exit    - выход из программы");
        System.out.println();
        System.out.println("  project-create - создание нового проекта");
        System.out.println("  project-list   - вывод списка проектов");
        System.out.println("  project-clear  - удаление всех проектов");
        System.out.println();
        System.out.println("  task-create - создание новой задачи");
        System.out.println("  task-list   - вывод списка задач");
        System.out.println("  task-clear  - удаление всех задач");
        System.out.println();
        System.out.println("  project-view-by-index - вывод информации проекта по индексу в хранилище");
        System.out.println("  project-view-by-name  - вывод информации проекта по имени");
        System.out.println("  project-view-by-id    - вывод информации проекта по идентификатору");
        System.out.println();
        System.out.println("  project-edit-by-index - изменение параметров проекта по индексу в хранилище");
        System.out.println("  project-edit-by-name  - изменение параметров проекта по имени");
        System.out.println("  project-edit-by-id    - изменение параметров проекта по идентификатору");
        System.out.println();
        System.out.println("  project-remove-by-index - удаление проекта по индексу в хранилище");
        System.out.println("  project-remove-by-name  - удаление проекта по имени");
        System.out.println("  project-remove-by-id    - удаление проекта по идентификатору");
        System.out.println();
        System.out.println("  task-view-by-index - вывод информации задачи по индексу в хранилище");
        System.out.println("  task-view-by-name  - вывод информации задачи по имени");
        System.out.println("  task-view-by-id    - вывод информации задачи по идентификатору");
        System.out.println();
        System.out.println("  task-edit-by-index - изменение параметров задачи по индексу в хранилище");
        System.out.println("  task-edit-by-name  - изменение параметров задачи по имени");
        System.out.println("  task-edit-by-id    - изменение параметров задачи по идентификатору");
        System.out.println();
        System.out.println("  task-remove-by-index - удаление задачи по индексу в хранилище");
        System.out.println("  task-remove-by-name  - удаление задачи по имени");
        System.out.println("  task-remove-by-id    - удаление задачи по идентификатору");
        System.out.println();
        System.out.println("  task-add-to-project-by-ids       - связывание задачи с проектом по идентификаторам");
        System.out.println("  task-remove-from-project-by-ids  - отсоединение задачи от проекта по идентификаторам");
        return 0;
    }

    /**
     * Сообщение об ошибочной команде
     */
    public int displayError() {
        System.out.println("Unknown command");
        return -1;
    }

    /**
     * Обработка команды выхода из программы
     *
     * @return результат вызова
     */
    public int exit() {
        System.out.println("Terminating program ...");
        System.exit(0);
        return 0;
    }

}
